import django_tables2 as tables
from django_tables2.utils import A
import itertools
from .models import VitalSigns, TurningSchedule, FluidIntakeAndOutput

class VitalSignsTable(tables.Table):
    blood_pressure = tables.Column(empty_values=())
    change = tables.LinkColumn('geriatrics:patient_monitoring:vs-update', args=[A('pk')],
            attrs={'a': {'class': 'update-vs btn btn-warning btn-sm'}}, text='Edit')
    remove = tables.LinkColumn('geriatrics:patient_monitoring:vs-delete', args=[A('pk')],
            attrs={'a': {'class': 'delete-vs btn btn-danger btn-sm'}}, text='Delete')

    class Meta:
        model = VitalSigns
        template_name = 'core/bootstrap.html'
        sequence = ('date', 'time', 'blood_pressure',)
        exclude = ('id', 'admission', 'systolic', 'diastolic',)

    def render_blood_pressure(self, record):
        return f'{record.systolic}/{record.diastolic} mmHg'

class FluidIntakeAndOutputTable(tables.Table):
   change = tables.LinkColumn('geriatrics:patient_monitoring:fluids-update', args=[A('pk')],
            attrs={'a': {'class': 'update-fluids btn btn-warning btn-sm'}}, text='Edit')
   remove = tables.LinkColumn('geriatrics:patient_monitoring:fluids-delete', args=[A('pk')],
            attrs={'a': {'class': 'delete-fluids btn btn-danger btn-sm'}}, text='Delete')

   class Meta:
        model = FluidIntakeAndOutput
        template_name = 'core/bootstrap.html'
        exclude = ('id', 'admission',)

class TurningScheduleTable(tables.Table):
    change = tables.LinkColumn('geriatrics:patient_monitoring:turning-update', args=[A('pk')],
            attrs={'a': {'class': 'update-turning btn btn-warning btn-sm'}}, text='Edit')
    remove = tables.LinkColumn('geriatrics:patient_monitoring:turning-delete', args=[A('pk')],
            attrs={'a': {'class': 'delete-turning btn btn-danger btn-sm'}}, text='Delete')

    class Meta:
        model = TurningSchedule
        template_name = 'core/bootstrap.html'
        fields = ('date', 'am_6', 'am_7', 'am_8', 'am_9', 'am_10', 'am_11', 'noon',
                'pm_1', 'pm_2', 'pm_3', 'pm_4', 'pm_5', 'pm_6', 'pm_7', 'pm_8', 'pm_9',
                'pm_10', 'pm_11', 'midnight', 'am_1', 'am_2', 'am_3', 'am_4', 'am_5', 'added_by',)
        exclude = ('id', 'admission',)


