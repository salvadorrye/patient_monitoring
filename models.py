from django.db import models
from django.conf import settings
from geriatrics.models import Admission

SHIFT = (
    ('6-6p', '6am-6pm'),
    ('6-6a', '6pm-6am'),
    ('6-2p', '6am-2pm'),
    ('2-10', '2pm-10pm'),
    ('10-6', '10pm-6am'),
)

POSITION = (
    ('0', 'Left side-lying'),
    ('1', 'Right side-lying'),
    ('2', 'Supine'),
    ('3', 'Low Fowler\'s'),
    ('4', 'Semi-Fowler\'s'),
    ('5', 'High Fowler\'s'),
    ('6', 'Chair'),
    ('7', 'Wheelchair'),
)

# Create your models here.
class VitalSigns(models.Model):
    admission = models.ForeignKey(Admission, on_delete=models.CASCADE, limit_choices_to={'discharged': False})
    date = models.DateField(auto_now=False, null=False, blank=False)
    time = models.TimeField(auto_now=False, null=False, blank=False)
    temperature = models.DecimalField(max_digits=3, decimal_places=1)
    pulse_rate = models.PositiveSmallIntegerField() 
    respiratory_rate = models.PositiveSmallIntegerField()
    systolic = models.PositiveSmallIntegerField()
    diastolic = models.PositiveSmallIntegerField()
    oxygen_saturation = models.PositiveSmallIntegerField(null=True, blank=True)
    cbg = models.PositiveSmallIntegerField(verbose_name='CBG', null=True, blank=True)
    gcs = models.PositiveSmallIntegerField(verbose_name='GCS', null=True, blank=True)
    weight = models.DecimalField(verbose_name='Weight in kg', max_digits=3, decimal_places=1, null=True, blank=True)
    added_by = models.ForeignKey(settings.AUTH_USER_MODEL,
            null=True, blank=True, on_delete=models.SET_NULL)

    class Meta:
        ordering = ['-date', '-time',]
        verbose_name_plural = 'Vital signs'

    def __str__(self):
        return '{2} ({0} {1})'.format(self.date.strftime('%B %d, %Y'), self.time.strftime('%I:%M %p'), self.admission)

class FluidIntakeAndOutput(models.Model):
    admission = models.ForeignKey(Admission, on_delete=models.CASCADE, limit_choices_to={'discharged': False})
    date = models.DateField(auto_now=False, null=False, blank=False)
    shift = models.CharField(max_length=4, choices=SHIFT)
    intravenous_fluids = models.PositiveSmallIntegerField(null=True, blank=True) 
    oral = models.PositiveSmallIntegerField(null=True, blank=True)
    urine_measured = models.PositiveSmallIntegerField(null=True, blank=True)
    urine_number_of_times = models.PositiveSmallIntegerField(null=True, blank=True)
    drainage = models.PositiveSmallIntegerField(null=True, blank=True)
    bowel_movement = models.PositiveSmallIntegerField(null=True, blank=True)
    added_by = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)

    class Meta:
        ordering = ['-date', 'shift']
        verbose_name_plural = 'Fluid intake and output'

    def __str__(self):
        return '{2} ({0} {1})'.format(self.date.strftime('%B %d, %Y'), self.shift, self.admission)

class TurningSchedule(models.Model):
    admission = models.ForeignKey(Admission, on_delete=models.CASCADE, limit_choices_to={'discharged': False})
    date = models.DateField(auto_now=False, null=False, blank=False)
    am_6 = models.CharField(max_length=2, choices=POSITION, verbose_name='6am', blank=True)
    am_7 = models.CharField(max_length=2, choices=POSITION, verbose_name='7am', blank=True) 
    am_8 = models.CharField(max_length=2, choices=POSITION, verbose_name='8am', blank=True)
    am_9 = models.CharField(max_length=2, choices=POSITION, verbose_name='9am', blank=True)
    am_10 = models.CharField(max_length=2, choices=POSITION, verbose_name='10am', blank=True)
    am_11 = models.CharField(max_length=2, choices=POSITION, verbose_name='11am', blank=True)
    noon = models.CharField(max_length=2, choices=POSITION, blank=True)
    pm_1 = models.CharField(max_length=2, choices=POSITION, verbose_name='1pm', blank=True)
    pm_2 = models.CharField(max_length=2, choices=POSITION, verbose_name='2pm', blank=True)
    pm_3 = models.CharField(max_length=2, choices=POSITION, verbose_name='3pm', blank=True)
    pm_4 = models.CharField(max_length=2, choices=POSITION, verbose_name='4pm', blank=True)
    pm_5 = models.CharField(max_length=2, choices=POSITION, verbose_name='5pm', blank=True)
    pm_6 = models.CharField(max_length=2, choices=POSITION, verbose_name='6pm', blank=True)
    pm_7 = models.CharField(max_length=2, choices=POSITION, verbose_name='7pm', blank=True)
    pm_8 = models.CharField(max_length=2, choices=POSITION, verbose_name='8pm', blank=True)
    pm_9 = models.CharField(max_length=2, choices=POSITION, verbose_name='9pm', blank=True)
    pm_10 = models.CharField(max_length=2, choices=POSITION, verbose_name='10pm', blank=True)
    pm_11 = models.CharField(max_length=2, choices=POSITION, verbose_name='11pm', blank=True)
    midnight = models.CharField(max_length=2, choices=POSITION, blank=True)
    am_1 = models.CharField(max_length=2, choices=POSITION, verbose_name='1am', blank=True)
    am_2 = models.CharField(max_length=2, choices=POSITION, verbose_name='2am', blank=True)
    am_3 = models.CharField(max_length=2, choices=POSITION, verbose_name='3am', blank=True)
    am_4 = models.CharField(max_length=2, choices=POSITION, verbose_name='4am', blank=True)
    am_5 = models.CharField(max_length=2, choices=POSITION, verbose_name='5am', blank=True)
    added_by = models.ManyToManyField(settings.AUTH_USER_MODEL)

    class Meta:
        ordering = ['-date',]

    def __str__(self):
        return '{1} ({0})'.format(self.date.strftime('%B %d, %Y'), self.admission)

