from django.urls import path
from . import views

app_name = 'patient_monitoring'

urlpatterns = [
    path('vs/<int:pk>', views.VitalSignsList.as_view(), name='vs-list'),
    path('vs/create/', views.VitalSignsCreate.as_view(), name='vs-create'),
    path('vs/create/<int:pk>', views.VitalSignsCreate.as_view(), name='vs-create'),
    path('vs/<int:pk>/update', views.VitalSignsUpdate.as_view(), name='vs-update'),
    path('vs/<int:pk>/delete', views.VitalSignsDelete.as_view(), name='vs-delete'),
    path('fluids/<int:pk>', views.FluidIntakeAndOutputList.as_view(), name='fluids-list'),
    path('fluids/create/', views.FluidIntakeAndOutputCreate.as_view(), name='fluids-create'),
    path('fluids/create/<int:pk>', views.FluidIntakeAndOutputCreate.as_view(), name='fluids-create'),
    path('fluids/<int:pk>/update', views.FluidIntakeAndOutputUpdate.as_view(), name='fluids-update'),
    path('fluids/<int:pk>/delete', views.FluidIntakeAndOutputDelete.as_view(), name='fluids-delete'),
    path('turning/<int:pk>', views.TurningScheduleList.as_view(), name='turning-list'),
    path('turning/create/<int:pk>', views.TurningScheduleCreate.as_view(), name='turning-create'),
    path('turning/<int:pk>/update', views.TurningScheduleUpdate.as_view(), name='turning-update'),
    path('turning/<int:pk>/delete', views.TurningScheduleDelete.as_view(), name='turning-delete'),
]
