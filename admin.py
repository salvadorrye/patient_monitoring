from django.contrib import admin
from django.contrib.admin import AdminSite
from .models import VitalSigns, FluidIntakeAndOutput, TurningSchedule

class PatientMonitoringAdminSite(AdminSite):
    site_header = 'JHWC Patient Monitoring Admin'
    site_title = 'JHWC Patient Monitoring Admin Portal'
    index_title = 'Welcome to JHWC Patient Monitoring Portal'
    
    def get_app_list(self, request):
        """
        Return a sorted list of all the installed apps that have been
        registered in this site.
        """
        ordering = {
            "Vital signs": 1,
            "Fluid intake and output": 2,
            "Turning schedules": 3
        }
        app_dict = self._build_app_dict(request)
        # a.sort(key=lambda x: b.index(x[0]))
        # Sort the apps alphabetically.
        app_list = sorted(app_dict.values(), key=lambda x: x['name'].lower())

        # Sort the models alphabetically within each app.
        for app in app_list:
            app['models'].sort(key=lambda x: ordering[x['name']])

        return app_list


patient_monitoring_admin_site = PatientMonitoringAdminSite(name='patient_monitoring_admin')

# Register your models here.
@admin.register(VitalSigns)
class VitalSignsAdmin(admin.ModelAdmin):
    list_display = ['date', 'time', 'admission', 'temperature', 'pulse_rate',
                    'respiratory_rate', 'systolic', 'diastolic', 'oxygen_saturation',
                    'cbg', 'gcs', 'weight', 'added_by']
    list_filter = ['date']
    exclude = ['added_by',]

    def save_model(self, request, obj, form, change):
        obj.added_by = request.user
        super().save_model(request, obj, form, change)

@admin.register(FluidIntakeAndOutput)
class FluidIntakeAndOutputAdmin(admin.ModelAdmin):
    list_display = ['date', 'shift', 'admission', 'intravenous_fluids', 'oral', 'total_input', 
                    'urine_measured', 'urine_number_of_times', 'drainage', 
                    'total_output', 'bowel_movement', 'added_by']
    list_filter = ['shift', 'date']
    fieldsets = (
            ('Date/Shift', {'fields': ('date', 'shift', 'admission',)}),
            ('Intake', {'fields': ('intravenous_fluids', 'oral',)}),
            ('Output', {'fields': ('urine_measured', 'urine_number_of_times', 'drainage', 'bowel_movement',)}),
        )
    exclude = ['added_by']

    def total_input(self, obj):
        if obj.intravenous_fluids and obj.oral:
            return obj.intravenous_fluids + obj.oral
        elif obj.intravenous_fluids:
            return obj.intravenous_fluids
        elif obj.oral:
            return obj.oral
        else:
            return '-'

    def total_output(self, obj):
        if obj.urine_measured and obj.drainage:
            return obj.urine_measured + obj.drainage
        elif obj.urine_measured:
            return obj.urine_measured
        elif obj.drainage:
            return obj.drainage
        else:
            return '-'

    def save_model(self, request, obj, form, change):
        obj.added_by = request.user
        super().save_model(request, obj, form, change)

@admin.register(TurningSchedule)
class TurningScheduleAdmin(admin.ModelAdmin):
    list_display = ['date', 'admission', 'am_6', 'am_7', 'am_8', 'am_9', 'am_10', 'am_11', 'noon',
            'pm_1', 'pm_2', 'pm_3', 'pm_4', 'pm_5', 'pm_6', 'pm_7', 'pm_8', 'pm_9', 'pm_10', 'pm_11',
            'midnight', 'am_1', 'am_2', 'am_3', 'am_4', 'am_5', 'added_by_display']
    list_filter = ['date']
    fieldsets = (
            ('Date/Patient', {'fields': ('date', 'admission', 'added_by',)}),
            ('6am-6pm', {'fields': ('am_6', 'am_7', 'am_8', 'am_9', 'am_10', 'am_11', 'noon',
            'pm_1', 'pm_2', 'pm_3', 'pm_4', 'pm_5',)}),
            ('6pm-6am', {'fields': ('pm_6', 'pm_7', 'pm_8', 'pm_9', 'pm_10', 'pm_11',
            'midnight', 'am_1', 'am_2', 'am_3', 'am_4', 'am_5',)}),
    )

    def added_by_display(self, obj):
        return ", ".join([
            u.username for u in obj.added_by.all()
        ])
    added_by_display.short_description = 'Added by'

patient_monitoring_admin_site.register(VitalSigns, admin_class=VitalSignsAdmin)
patient_monitoring_admin_site.register(FluidIntakeAndOutput, admin_class=FluidIntakeAndOutputAdmin)
patient_monitoring_admin_site.register(TurningSchedule, admin_class=TurningScheduleAdmin)
