from django import forms
from django.forms import ModelForm
from .models import VitalSigns, FluidIntakeAndOutput, TurningSchedule
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit, Layout, Hidden, Field
from bootstrap_modal_forms.forms import BSModalModelForm

class DateInput(forms.DateInput):
    input_type = 'date'

class TimeInput(forms.TimeInput):
    input_type = 'time'

class VitalSignsCreateForm(BSModalModelForm):
    class Meta:
        model = VitalSigns
        fields = '__all__' 
        widgets = { 'date': DateInput(),
                    'time': TimeInput(),
                  }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_id = 'vs-form'
        self.helper.form_class = 'white-forms'
        #self.helper.add_input(Submit('submit', 'Submit'))

class FluidIntakeAndOutputCreateForm(BSModalModelForm):
    class Meta:
        model = FluidIntakeAndOutput
        fields = '__all__'
        widgets = { 'date': DateInput(),}
       
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_id = 'fluids-form'
        self.helper.form_class = 'white-forms'

class TurningScheduleCreateForm(BSModalModelForm):
    class Meta:
        model = TurningSchedule
        fields = '__all__'
        widgets = { 'date': DateInput(),}
       
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_id = 'turning-form'
        self.helper.form_class = 'white-forms'

