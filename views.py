from django.views.generic import ListView
from django.views.generic.edit import CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy
from django.contrib.auth.mixins import LoginRequiredMixin

from .models import VitalSigns, FluidIntakeAndOutput, TurningSchedule
from .forms import VitalSignsCreateForm, FluidIntakeAndOutputCreateForm, TurningScheduleCreateForm
from geriatrics.models import Admission

from patient_monitoring.tables import VitalSignsTable, FluidIntakeAndOutputTable, TurningScheduleTable

from bootstrap_modal_forms.generic import BSModalCreateView, BSModalUpdateView, BSModalDeleteView

class VitalSignsList(LoginRequiredMixin, ListView):
    model = VitalSigns
    template_name = 'patient_monitoring/includes/vital_signs.html'

    def get_context_data(self, *args, **kwargs):
        request = self.request
        context = super(VitalSignsList, self).get_context_data(**kwargs)
        if 'pk' in self.kwargs:
            try:
                admission = Admission.objects.get(id=self.kwargs['pk'])
                context['admission'] = admission
                data = VitalSigns.objects.filter(admission=admission)
                if data:
                    vs_table = VitalSignsTable(data)
                    vs_table.paginate(page=request.GET.get("page", 1), per_page=5)
                    context['table'] = vs_table
                else:
                    context['table'] = None
            except Admission.DoesNotExist:
                pass
        return context

class FluidIntakeAndOutputList(LoginRequiredMixin, ListView):
    model = FluidIntakeAndOutput
    template_name = 'patient_monitoring/includes/fluid_intake_and_output.html'

    def get_context_data(self, *args, **kwargs):
        request = self.request
        context = super(FluidIntakeAndOutputList, self).get_context_data(**kwargs)
        if 'pk' in self.kwargs:
            try:
                admission = Admission.objects.get(id=self.kwargs['pk'])
                data = FluidIntakeAndOutput.objects.filter(admission=admission)
                if data:
                    fluids_table = FluidIntakeAndOutputTable(data)
                    fluids_table.paginate(page=request.GET.get("page", 1), per_page=5)
                    context['table'] = fluids_table
                else:
                    context['table'] = None
            except Admission.DoesNotExist:
                pass
        return context

class TurningScheduleList(LoginRequiredMixin, ListView):
    model = TurningSchedule
    template_name = 'patient_monitoring/includes/turning_schedule.html'

    def get_context_data(self, *args, **kwargs):
        request = self.request
        context = super(TurningScheduleList, self).get_context_data(**kwargs)
        if 'pk' in self.kwargs:
            try:
                admission = Admission.objects.get(id=self.kwargs['pk'])
                data = TurningSchedule.objects.filter(admission=admission)
                if data:
                    turn_table = TurningScheduleTable(data)
                    turn_table.paginate(page=request.GET.get("page", 1), per_page=5)
                    context['table'] = turn_table
                else:
                    context['table'] = None
            except Admission.DoesNotExist:
                pass
        return context

class VitalSignsCreate(LoginRequiredMixin, BSModalCreateView):
    model = VitalSigns
    form_class = VitalSignsCreateForm
    success_message = 'Vital Signs record was created.'
    success_url = reverse_lazy('geriatrics:admissions')
 
    def get_form(self, *args, **kwargs):
        form = super(VitalSignsCreate, self).get_form(*args, **kwargs)
        if 'pk' in self.kwargs:
            try:
                admission = Admission.objects.get(id=self.kwargs['pk'])
                form.fields['admission'].initial = admission
                form.fields['added_by'].initial = self.request.user
                self.success_url = reverse_lazy('geriatrics:admission-detail', kwargs={'pk': admission.id,})
            except Admission.DoesNotExist:
                pass
        return form

    def get_context_data(self, **kwargs):
        context = super(VitalSignsCreate, self).get_context_data(**kwargs)
        context['title'] = 'Create Vital Signs'
        context['btn'] = 'Create'
        return context

class FluidIntakeAndOutputCreate(LoginRequiredMixin, BSModalCreateView):
    model = FluidIntakeAndOutput
    form_class = FluidIntakeAndOutputCreateForm
    success_message = 'Fluid Intake and Output record was created.'
    success_url = reverse_lazy('geriatrics:admissions')
 
    def get_form(self, *args, **kwargs):
        form = super(FluidIntakeAndOutputCreate, self).get_form(*args, **kwargs)
        if 'pk' in self.kwargs:
            try:
                admission = Admission.objects.get(id=self.kwargs['pk'])
                form.fields['admission'].initial = admission
                form.fields['added_by'].initial = self.request.user
                self.success_url = reverse_lazy('geriatrics:admission-detail', kwargs={'pk': admission.id,})
            except Admission.DoesNotExist:
                pass
        return form

    def get_context_data(self, **kwargs):
        context = super(FluidIntakeAndOutputCreate, self).get_context_data(**kwargs)
        context['title'] = 'Create Fluid Intake and Output'
        context['btn'] = 'Create'
        return context

class TurningScheduleCreate(LoginRequiredMixin, BSModalCreateView):
    model = TurningSchedule
    form_class = TurningScheduleCreateForm
    success_message = 'Turning Schedule record was created.'
    success_url = reverse_lazy('geriatrics:admissions')
 
    def get_form(self, *args, **kwargs):
        form = super(TurningScheduleCreate, self).get_form(*args, **kwargs)
        if 'pk' in self.kwargs:
            try:
                admission = Admission.objects.get(id=self.kwargs['pk'])
                form.fields['admission'].initial = admission
                form.fields['added_by'].initial = self.request.user
                self.success_url = reverse_lazy('geriatrics:admission-detail', kwargs={'pk': admission.id,})
            except Admission.DoesNotExist:
                pass
        return form

    def get_context_data(self, **kwargs):
        context = super(TurningScheduleCreate, self).get_context_data(**kwargs)
        context['title'] = 'Create Turning Schedule'
        context['btn'] = 'Create'
        return context

class VitalSignsUpdate(LoginRequiredMixin, BSModalUpdateView):
    model = VitalSigns
    form_class = VitalSignsCreateForm
    template_name = 'patient_monitoring/vitalsigns_form.html'
    success_message = 'Vital Signs record was updated.'
    success_url = reverse_lazy('geriatrics:admissions')

    def get_object(self, *args, **kwargs):
        vs = super(VitalSignsUpdate, self).get_object(*args, **kwargs)
        admission = vs.admission
        #added_by = self.request.user
        self.success_url = reverse_lazy('geriatrics:admission-detail', kwargs={'pk': admission.id,})
        return vs

    def get_context_data(self, **kwargs):
        context = super(VitalSignsUpdate, self).get_context_data(**kwargs)
        context['title'] = 'Update Vital Signs'
        context['btn'] = 'Update'
        return context

class FluidIntakeAndOutputUpdate(LoginRequiredMixin, BSModalUpdateView):
    model = FluidIntakeAndOutput
    form_class = FluidIntakeAndOutputCreateForm
    template_name = 'patient_monitoring/fluidintakeandoutput_form.html'
    success_message = 'Fluid Intake and Output record was updated.'
    success_url = reverse_lazy('geriatrics:admissions')

    def get_object(self, *args, **kwargs):
        vs = super(FluidIntakeAndOutputUpdate, self).get_object(*args, **kwargs)
        admission = vs.admission
        #added_by = self.request.user
        self.success_url = reverse_lazy('geriatrics:admission-detail', kwargs={'pk': admission.id,})
        return vs

    def get_context_data(self, **kwargs):
        context = super(FluidIntakeAndOutputUpdate, self).get_context_data(**kwargs)
        context['title'] = 'Update Fluid Intake and Output'
        context['btn'] = 'Update'
        return context

class TurningScheduleUpdate(LoginRequiredMixin, BSModalUpdateView):
    model = TurningSchedule
    form_class = TurningScheduleCreateForm
    template_name = 'patient_monitoring/turningschedule_form.html'
    success_message = 'Turning Schedule record was updated.'
    success_url = reverse_lazy('geriatrics:admissions')

    def get_object(self, *args, **kwargs):
        vs = super(TurningScheduleUpdate, self).get_object(*args, **kwargs)
        admission = vs.admission
        self.success_url = reverse_lazy('geriatrics:admission-detail', kwargs={'pk': admission.id,})
        return vs

    def get_context_data(self, **kwargs):
        context = super(TurningScheduleUpdate, self).get_context_data(**kwargs)
        context['title'] = 'Update Turning Schedule'
        context['btn'] = 'Update'
        return context

class VitalSignsDelete(LoginRequiredMixin, BSModalDeleteView):
    model = VitalSigns
    success_message = 'Vital Signs record deleted.'
    success_url = reverse_lazy('geriatrics:admissions')

    def get_object(self, *args, **kwargs):
        vs = super(VitalSignsDelete, self).get_object(*args, **kwargs)
        admission = vs.admission
        self.success_url = reverse_lazy('geriatrics:admission-detail', kwargs={'pk': admission.id,})
        return vs

    def get_context_data(self, **kwargs):
        context = super(VitalSignsDelete, self).get_context_data(**kwargs)
        context['title'] = 'Delete Vital Signs'
        return context

class FluidIntakeAndOutputDelete(LoginRequiredMixin, BSModalDeleteView):
    model = FluidIntakeAndOutput
    success_message = 'Fluid Intake and Output record deleted.'
    success_url = reverse_lazy('geriatrics:admissions')

    def get_object(self, *args, **kwargs):
        vs = super(FluidIntakeAndOutputDelete, self).get_object(*args, **kwargs)
        admission = vs.admission
        self.success_url = reverse_lazy('geriatrics:admission-detail', kwargs={'pk': admission.id,})
        return vs

    def get_context_data(self, **kwargs):
        context = super(FluidIntakeAndOutputDelete, self).get_context_data(**kwargs)
        context['title'] = 'Delete Fluid Intake and Output'
        return context

class TurningScheduleDelete(LoginRequiredMixin, BSModalDeleteView):
    model = TurningSchedule
    success_message = 'Turning Schedule record deleted.'
    success_url = reverse_lazy('geriatrics:admissions')

    def get_object(self, *args, **kwargs):
        vs = super(TurningScheduleDelete, self).get_object(*args, **kwargs)
        admission = vs.admission
        self.success_url = reverse_lazy('geriatrics:admission-detail', kwargs={'pk': admission.id,})
        return vs

    def get_context_data(self, **kwargs):
        context = super(TurningScheduleDelete, self).get_context_data(**kwargs)
        context['title'] = 'Delete Turning Schedule'
        return context

