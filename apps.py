from django.apps import AppConfig


class PatientMonitoringConfig(AppConfig):
    name = 'patient_monitoring'
