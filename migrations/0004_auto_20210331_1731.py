# Generated by Django 3.1.6 on 2021-03-31 09:31

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('patient_monitoring', '0003_turningschedule'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='fluidintakeandoutput',
            options={'ordering': ['-date', 'shift'], 'verbose_name_plural': 'Fluid Intakes and Outputs'},
        ),
        migrations.AlterModelOptions(
            name='turningschedule',
            options={'ordering': ['-date']},
        ),
        migrations.AlterModelOptions(
            name='vitalsigns',
            options={'ordering': ['-date', '-time'], 'verbose_name_plural': 'Vital signs'},
        ),
    ]
